package sg.threads;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class BlockingQueue {

	private List queue = new LinkedList();
	private int limit = 10;

	public BlockingQueue(int limit) {
		this.limit = limit;
	}

	public synchronized void enqueue(Object item) throws InterruptedException {
		while (this.queue.size() == this.limit) {
			System.out.println("limit reached waiting...");
			wait();
			System.out.println("wake up after limit reached waiting...");
		}
//		if (this.queue.size() == 0) {
//			System.out.println("Notify all in enqueue as size = 0");
//			notifyAll();
//		}
		notifyAll();
		this.queue.add(item);
	}

	public synchronized Object dequeue() throws InterruptedException {
		while (this.queue.size() == 0) {
			System.out.println("empty queue waiting...");
			wait();
			System.out.println("wake up  empty queue waiting...");
		}
//		if (this.queue.size() == this.limit) {
//			System.out.println("Notify all in enqueue as size = "+this.limit);
//			notifyAll();
//		}
		notifyAll();
		return this.queue.remove(0);
	}
	
	public static void main(String[] args) {
		final BlockingQueue bq = new BlockingQueue(10);
		final int sleepMaxP = 5*1000;
		final int sleepMaxC = 1000;
		
		Thread producer = new Thread(new Runnable() {

			Random rp = new Random();
			@Override
			public void run() {
				
				try {
					while (true) {
					int rand = rp.nextInt(sleepMaxP);
					Thread.sleep(rp.nextInt(rand));
					bq.enqueue(rand+"");
					System.out.println("producer: "+rand);
					}
				} catch (InterruptedException e) {
					
					e.printStackTrace();
				}
				
			}
			
		});
		
		
		Thread consumer = new Thread(new Runnable() {

			Random rp = new Random();
			@Override
			public void run() {
				
				try {
					while(true) {
						int rand = rp.nextInt(sleepMaxC);
						Thread.sleep(rp.nextInt(rand));
						String str = (String) bq.dequeue();
						System.out.println("consumer: "+str);
					}
					
				} catch (InterruptedException e) {
					
					e.printStackTrace();
				}
				
			}
			
		});
		
		producer.start();
		consumer.start();
	}

}
