package sg.books.tij.generics.example1;

public interface Generator<T> {
	public T next();
}
