package sg.generics;

public class Stack<T> {
	T obj;
	
	public void set(T o) {
		obj = o;
	}
	
	public T get() {
		return obj;
	}
	
	public static void main(String[] args) {
		Stack<Integer>  stk = new Stack<>();
		stk.set(10);
		System.out.println(stk.get());
	}
	
}



  class Shape {
  void draw(Canvas c){}
}
 class Circle extends Shape {
private int x, y, radius;
public void draw(Canvas c) {  }
}
 class Rectangle extends Shape {
private int x, y, width, height;
public void draw(Canvas c) {  }
}
//These classes can be drawn on a canvas:
 class Canvas {
public void draw(Shape s) {
s.draw(this);
}
}