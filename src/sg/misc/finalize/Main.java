package sg.misc.finalize;

public class Main {

public static void main(String[] args) throws InterruptedException {
	DerivedTest t1 = new DerivedTest();
	t1 = null;
	System.gc();
	Thread t = new Thread(new Runnable() {

		@Override
		public void run() {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

		}

	});

	t.start();
	t.join();
	System.out.println("program ended");
}
}
