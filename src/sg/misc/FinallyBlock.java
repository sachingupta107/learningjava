package sg.misc;

public class FinallyBlock {
	static int value = 1;

	public static void main(String[] args) {
		FinallyBlock test1 = new FinallyBlock();
		System.out.println(test1.getValue());
		System.out.println(FinallyBlock.value);
	}

	public int getValue() {
		try {
			value = value + 1;
			return value;
		} catch (Exception e) {
			return 0;
		} finally {
			value = value + 1;
		}
	}

}
