//shows constructor chaining
package sg.misc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class Cube {

    int length;
    int breadth;
    int height;
    public int getVolume() {
        return (length * breadth * height);
    }
    Cube() {
        this(10, 10);
        System.out.println("Finished with Default Constructor of Cube");
    }
    Cube(int l, int b) {
        this(l, b, 10);
        System.out.println("Finished with Parameterized Constructor having 2 params of Cube");
    }
    Cube(int l, int b, int h) {
    	System.out.println("3 params cube constructor");
        length = l;
        breadth = b;
        height = h;
    }
}

class SpecialCube extends Cube implements Serializable {

    int weight;
    SpecialCube() {
        super();
        weight = 10;
    }
    SpecialCube(int l, int b) {
        this(l, b, 10);
        System.out.println("Finished with Parameterized Constructor having  2 params of SpecialCube");
    }
    SpecialCube(int l, int b, int h) {
        super(l, b, h);
        weight = 20;
        System.out.println("Finished with Parameterized Constructor having 3 params of SpecialCube");
    }
   
}

public class ConstructorChaining {
	 public static void main(String[] args) {
		 SpecialCube specialObj1 = new SpecialCube();
		 SpecialCube specialObj2 = new SpecialCube(10, 20);
		 
		 ByteArrayOutputStream bos = new ByteArrayOutputStream();
		 byte[] objBytes = null;
		 ObjectOutputStream oos;
			try {
				oos = new ObjectOutputStream(bos);
				oos.writeObject(new SpecialCube());
				objBytes = bos.toByteArray();
				
				ObjectInputStream ois = new ObjectInputStream (new ByteArrayInputStream(objBytes));
				
				specialObj1 = (SpecialCube) ois.readObject();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
	        System.out.println("Volume of SpecialCube1 is : "
	                + specialObj1.getVolume());
//	        System.out.println("Weight of SpecialCube1 is : "
//	                + specialObj1.weight);
//	        System.out.println("Volume of SpecialCube2 is : "
//	                + specialObj2.getVolume());
//	        System.out.println("Weight of SpecialCube2 is : "
//	                + specialObj2.weight);
		 
		
		  
	    }
}