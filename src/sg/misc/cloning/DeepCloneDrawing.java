package sg.misc.cloning;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class DeepCloneDrawing {
	
	public static Drawing deepCloneDrawingSerial(Drawing draw) throws IOException,
			ClassNotFoundException {
		// Write to output stream
		
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bout);
		
//		File file = File.createTempFile("pre", "suf");
//		FileOutputStream fos = new FileOutputStream(file.getAbsoluteFile());
//		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(draw);
		
		byte[] bytes = bout.toByteArray();
		
		oos.close();
		bout.close();
		
		
//		oos.close();
//		fos.close();

		// Read back
		ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
		ObjectInputStream ois = new ObjectInputStream(bis);
		Drawing ret = (Drawing) ois.readObject();
		
//		FileInputStream fis = new FileInputStream(file.getAbsoluteFile());
//		ObjectInputStream ois = new ObjectInputStream(fis);
//		Drawing ret = (Drawing) ois.readObject();
//
//		ois.close();
//		fis.close();

		ois.close();
		bis.close();
		
		return ret;

	}

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Drawing d = new Drawing();
		
		
		for (int i = 0; i < 3; i++) {
			
			if (i % 2 == 0)
				d.shapes.add(new Rectangle());
			else
				d.shapes.add(new Circle());

		}
		System.out.println("before clone");
		for (Shape s: d.shapes) {
			System.out.println(s.getClass()+" "+s);
		}
		
		Drawing dc = (Drawing)d.clone();
		
		System.out.println("after clone");
		for (Shape s: dc.shapes) {
			System.out.println(s.getClass()+" "+s);
		}
	}
	
	public static void cloneTest() {

		HashMap<String, List<String>> cities = new HashMap<String, List<String>>();
		cities.put("France", Arrays.asList("Paris", "Grenoble"));
		HashMap<String, List<String>> citiesClone = (HashMap) cities.clone();
		citiesClone.get("France").set(0, "Dublin");
		System.out.println(cities.get("France"));
		
		System.out.println(cities.get("france") == citiesClone.get("france"));
		
		System.out.println(cities.hashCode());
		
		System.out.println(citiesClone.hashCode());
		
		
		List<String> cities1 = new LinkedList<String>();
		cities1.add(new String("c1"));
		cities1.add(new String("c2"));
		cities1.add(new String("c3"));
		System.out.println(cities1);
	
		List<String> cities2 = (LinkedList<String>) ((LinkedList<String>) cities1).clone();
		System.out.println(cities2);
		
		System.out.println(cities1 == cities2);
	}
}
