package sg.misc.cloning;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Drawing implements Serializable, Cloneable {
	
	public List<Shape> shapes = new LinkedList<Shape>();
	public Object clone() {
		Drawing dc = null;
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos;
		byte[] bytes;
		
		ByteArrayInputStream bis;
		ObjectInputStream ois;
		
		try {
			oos = new ObjectOutputStream(bos);
			oos.writeObject(this);
			bytes = bos.toByteArray();
			
			bis = new ByteArrayInputStream(bytes);
			ois = new ObjectInputStream(bis);
			
			dc = (Drawing)ois.readObject();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
		}
		
		return dc;
		
	}
}
