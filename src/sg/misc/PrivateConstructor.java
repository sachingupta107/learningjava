package sg.misc;

public class PrivateConstructor {

}


class A {
	private A() {
		
	}
	
	public A(int a) {
		
	}
}

class B extends A {
	public B() {
		super(8);
	}
}