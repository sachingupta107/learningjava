/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sample.threads.problem1;

/**
 *
 * @author sachin
 */
public class ProducerConsumerExample {
    public static void main(String[] args) {
        Drop drop = new Drop();
        Thread t1 = new Thread(new Producer(drop));
        t1.start();
        (new Thread(new Consumer(drop))).start();
    }
}
